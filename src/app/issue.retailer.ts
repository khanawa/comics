import { Issue } from './issue';


export class IssueRetailer{
    total: Number;
    limit: Number;
    skip: Number;
    data: Array<Issue>;
}