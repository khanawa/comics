import { Component, OnInit } from '@angular/core';
import { SupplierService } from '../supplier.service';
import { SupplierInfo } from '../supplier.info';

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.css']
})
export class SupplierComponent implements OnInit {

  supplierInfo: SupplierInfo;

  constructor(private supplierService: SupplierService) {}

  ngOnInit() {
    this.setSupplierInfo();
  }

  setSupplierInfo(): void{
    this.supplierService.getSupplierInfo().subscribe(supplierInfo => this.supplierInfo = supplierInfo);
  }

}
