import { Thumbnail } from './thumbanil';
import { Image } from './image';

export class Issue{
    title: String;
    series: String;
    description: String;
    publisherString: String;
    publicationDate: String;
    thumbnail: Thumbnail;
    images: Image[];
    _id: String;
}