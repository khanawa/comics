import { Issue } from './issue';
import { IssueRetailer } from './issue.retailer';



export const ISSUES: Issue[] = [
    {
      title: 'Captain America: Sam Wilson (2015)', 
      series: '7 (Cassaday Variant)',
      description: 'Captain America',
      publisherString: 'Marverl',
      publicationDate: '2016-01-15T21:22:34+02:00',
      thumbnail: {
       path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/90/56b8ac4c2709f',
       extension: 'jpg'
     },
     images: [{
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/90/56b8ac4c2709f',
      extension: 'jpg'
     },
     {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/4/e0/568e9097bf767',
      extension: 'jpg'
     },
     {
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/60/56058c4b7164b',
      extension: 'jpg'
     }
    
    ], 
       _id: '2WOcvJ4AGU0JDxsF' 
    },
    {
        title: 'Moon Girl and Devil Dinosaur (2015)',
        series: '5 (Guerra Wop Variant)',
        description: 'Moon Girl and Devil Dinosaur (2015)',
        publisherString: 'Marvel',
        publicationDate: '2016-01-15T21:22:35+02:00',
        thumbnail: {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
            extension: 'jpg'
          },
        images: [{
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/a0/56707df23de44',
            extension: 'jpg'
        }
        ],
        _id: '4ioKwYTA6uHeddpR'
    },
    {
        title: 'Spider-Man (2016)',
        series: '3',
        description: 'Miles finds himself face-to-face with his toughest villain yet- HIS GRANDMOTHER!!! And an Avengers-study session (date?) with Ms. Marvel goes horribly wrong!',
        publisherString: 'Marvel',
        publicationDate: '2016-01-14T19:12:25+02:00',
        thumbnail: {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/a0/5697d7ca0f513',
            extension: 'jpg'
          },
        images: [{
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/a0/5697d7ca0f513',
            extension: 'jpg'
        }
        ],
        _id: 'KWRnjZ5RUywxZtFi'
    }
];

export const ISSUE_RETAILER: IssueRetailer ={
  total: 3,
  limit: 10,
  skip: 0,
  data: ISSUES
};