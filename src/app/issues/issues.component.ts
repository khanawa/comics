import { Component, OnInit } from '@angular/core';
import { Issue } from '../issue';
import { IssueService } from '../issue.service';
import { IssueRetailer } from '../issue.retailer';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.css']
})
export class IssuesComponent implements OnInit {

  issues: Issue[];
  retailer: IssueRetailer;

  constructor(private issueService: IssueService) { }

  ngOnInit() {
    this.getIssues();
    this.getRetailer();
  }

  getIssues(): void{
    this.issueService.getIssues().subscribe(issues => this.issues = issues);
  }

  getRetailer(): void{
    this.issueService.getIssueRetailer().subscribe(retailer => this.retailer = retailer);
  }

}
