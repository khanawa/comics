import { Supplier } from './supplier';

export class SupplierInfo{
    total: Number;
    limit: Number;
    skip: Number;
    data: Array<Supplier>;
}