import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Issue } from './issue';
import { ISSUES } from './mock-issues';
import { IssueRetailer } from './issue.retailer';

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  private issuesUrl = 'http://localhost:3030/issues';

  constructor(private http: HttpClient) {
    
  }

  getIssues(): Observable <Issue[]>{
    return of(ISSUES);
  }

  getIssueRetailer(): Observable<IssueRetailer>{
    return this.http.get<IssueRetailer>(this.issuesUrl);
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
 
    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead
 
    // TODO: better job of transforming error for user consumption
   // this.log(`${operation} failed: ${error.message}`);
 
    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}
}


