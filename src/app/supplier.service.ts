import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SupplierInfo } from './supplier.info';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {

  private supplierUrl = 'http://localhost:3030/suppliers';

  constructor(private http: HttpClient) { }

  getSupplierInfo(): Observable<SupplierInfo>{
    return this.http.get<SupplierInfo>(this.supplierUrl);
  }
}
