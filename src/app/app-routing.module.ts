import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupplierComponent } from './supplier/supplier.component';
import { IssuesComponent } from './issues/issues.component';

const routes: Routes = [
  
  { path: 'suppliers', component: SupplierComponent },
  { path: 'issues', component: IssuesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
