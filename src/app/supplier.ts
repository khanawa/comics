export class Supplier{
    _id: String;
    name: String;
    city: String;
    reference: String;
    status: String;
}